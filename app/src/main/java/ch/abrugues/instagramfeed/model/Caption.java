package ch.abrugues.instagramfeed.model;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Caption implements Serializable {

    @SerializedName("text")
    private String text;

    public String getText() {
        return text;
    }
}
