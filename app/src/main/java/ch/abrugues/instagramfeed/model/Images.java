package ch.abrugues.instagramfeed.model;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Images implements Serializable {

    @SerializedName("low_resolution")
    private Image lowResolutionImage;

    @SerializedName("thumbnail")
    private Image thumbnail;

    @SerializedName("standard_resolution")
    Image standardResolution;

    public Image getLowResolutionImage() {
        return lowResolutionImage;
    }

    public Image getThumbnail() {
        return thumbnail;
    }

    public Image getStandardResolution() {
        return standardResolution;
    }

}
