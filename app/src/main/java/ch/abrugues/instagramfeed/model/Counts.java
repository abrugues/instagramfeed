package ch.abrugues.instagramfeed.model;


import com.google.gson.annotations.SerializedName;

public class Counts {

    @SerializedName("media")
    private String mediaCount;

    @SerializedName("follows")
    private String follows;

    @SerializedName("followed_by")
    private String followedBy;

    public String getMediaCount() {
        return mediaCount;
    }

    public String getFollows() {
        return follows;
    }

    public String getFollowedBy() {
        return followedBy;
    }
}
