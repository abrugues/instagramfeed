package ch.abrugues.instagramfeed.model;


import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataMedia {

    @SerializedName("data")
    private List<Media> media;

    @SerializedName("meta")
    private Meta meta;

    public List<Media> getMedia() {
        return media;
    }

    public Meta getMeta() {
        return meta;
    }
}
