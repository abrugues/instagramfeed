package ch.abrugues.instagramfeed.model;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Image implements Serializable {

    @SerializedName("url")
    private String url;

    @SerializedName("width")
    int width;

    @SerializedName("height")
    int height;

    public String getUrl() {
        return url;
    }
}
