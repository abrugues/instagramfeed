package ch.abrugues.instagramfeed.model;


import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("username")
    private String username;

    @SerializedName("profile_picture")
    private String profilePicture;

    @SerializedName("full_name")
    private String fullName;

    @SerializedName("counts")
    private Counts counts;

    public String getUsername() {
        return username;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public String getFullName() {
        return fullName;
    }

    public Counts getCounts() {
        return counts;
    }
}
