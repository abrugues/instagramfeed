package ch.abrugues.instagramfeed.model;

import com.google.gson.annotations.SerializedName;


public class Meta {

    @SerializedName("code")
    private int code;

    public int getCode() {
        return code;
    }
}
