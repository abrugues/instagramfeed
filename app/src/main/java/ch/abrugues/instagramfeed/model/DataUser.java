package ch.abrugues.instagramfeed.model;


import com.google.gson.annotations.SerializedName;

public class DataUser {

    @SerializedName("data")
    private User user;

    public User getUser() {
        return user;
    }
}
