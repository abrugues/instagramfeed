package ch.abrugues.instagramfeed.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Media implements Parcelable {

    @SerializedName("images")
    private Images images;

    @SerializedName("caption")
    private Caption caption;

    private Media(Parcel in) {
        images = (Images) in.readSerializable();
        caption = (Caption) in.readSerializable();
    }

    public Images getImages() {
        return images;
    }

    public Caption getCaption() {
        return caption;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Media> CREATOR = new Creator<Media>() {
        @Override
        public Media createFromParcel(Parcel in) {
            return new Media(in);
        }

        @Override
        public Media[] newArray(int size) {
            return new Media[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeSerializable(images);
        dest.writeSerializable(caption);
    }
}
