package ch.abrugues.instagramfeed.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import ch.abrugues.instagramfeed.R;
import ch.abrugues.instagramfeed.model.Media;

public class InstagramImageActivity extends AppCompatActivity {

    private static final String MEDIA_DETAILS = "MEDIA_DETAILS";

    @BindView(R.id.fullscreenPictureImgView) ImageView pictureImgView;
    @BindView(R.id.pictuteCaptionTxtView) TextView captionTxtView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instagram_image);

        ButterKnife.bind(this);

        Media media = (Media) getIntent().getExtras().get(MEDIA_DETAILS);

        String imageUrl = media.getImages().getStandardResolution().getUrl();
        Picasso.with(this).load(imageUrl).into(pictureImgView);

        String imageCaption = media.getCaption().getText();
        captionTxtView.setText(imageCaption);

    }

    public static Intent createIntent(Context context, Media media) {
        Intent i = new Intent(context, InstagramImageActivity.class);
        i.putExtra(MEDIA_DETAILS, media);
        return i;
    }
}
