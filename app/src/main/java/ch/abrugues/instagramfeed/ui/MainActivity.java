package ch.abrugues.instagramfeed.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.webkit.CookieManager;
import android.webkit.ValueCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import ch.abrugues.instagramfeed.R;

public class MainActivity extends AppCompatActivity {

    final static String TAG = "InstagramApp";

    private static final String GALLERY_FRAG = "GALLERY";
    private static final String PROFILE_FRAG = "PROFILE";

    static final String GALLERY_APP_PREFS = "GALLERY_APP_PREFS";
    static final String AUTH_TOKEN = "AUTH_TOKEN";

    @BindView(R.id.drawer_layout) DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    @BindView(R.id.navigation_view) NavigationView navigationView;

    private String authToken;
    private boolean mReturningWithResult = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setUpToolbar();
        setupDrawerLayout();
        getAuthToken();
    }

    @Override
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onPostResume() {
        super.onPostResume();
        setNavigationView();
        if (mReturningWithResult) {
            navigate(navigationView.getMenu().getItem(0));
        }
        mReturningWithResult = false;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return drawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }

    private void setUpToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void setupDrawerLayout() {
        drawerToggle = new ActionBarDrawerToggle(MainActivity.this, drawerLayout, R.string.app_name, R.string.app_name);
        drawerLayout.addDrawerListener(drawerToggle);
    }

    private void setNavigationView() {
        if (navigationView != null) {
            prepareDrawer();
            navigate(navigationView.getMenu().getItem(0));
        }
    }

    private void prepareDrawer() {
        Log.d(TAG, navigationView.toString());
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        navigate(item);
                        drawerLayout.closeDrawers();
                        return true;
                    }
                }
        );
    }

    private void navigate(MenuItem itemDrawer) {
        FragmentManager fm = getSupportFragmentManager();
        switch (itemDrawer.getItemId()) {
            case R.id.item_gallery:
                GalleryFragment galleryFragment = (GalleryFragment) fm.findFragmentByTag(GALLERY_FRAG);
                if (galleryFragment == null && !TextUtils.isEmpty(authToken)) {
                    galleryFragment = GalleryFragment.newInstance(authToken);
                    fm.beginTransaction()
                            .replace(R.id.main_container, galleryFragment, GALLERY_FRAG)
                            .commit();
                    setTitle(R.string.gallery);
                }
                break;
            case R.id.item_profile:
                ProfileFragment profileFragment = ProfileFragment.newInstance(authToken);
                fm.beginTransaction()
                        .replace(R.id.main_container, profileFragment, PROFILE_FRAG)
                        .commit();
                setTitle(R.string.profile);
                break;
            case R.id.item_logout:
                logout();
        }
    }

    private void getAuthToken() {
        SharedPreferences settings = getSharedPreferences(GALLERY_APP_PREFS, MODE_PRIVATE);
        authToken = settings.getString(AUTH_TOKEN, "");

        if (TextUtils.isEmpty(authToken)) {
            Intent i = new Intent(this, InstagramLoginActivity.class);
            startActivityForResult(i, InstagramLoginActivity.AUTHENTICATE_USER);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "Result received!");
        switch(resultCode) {
            case RESULT_OK:
                authToken = data.getExtras().getString(AUTH_TOKEN);
                SharedPreferences settings = getSharedPreferences(GALLERY_APP_PREFS, MODE_PRIVATE);
                SharedPreferences.Editor editor = settings.edit();
                editor.putString(AUTH_TOKEN, authToken);
                editor.apply();
                break;
            case RESULT_CANCELED:
                finish();
        }
        mReturningWithResult = true;
    }

    private void logout() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.dialog_logout_title)
                .setMessage(R.string.dialog_logout_message)
                .setPositiveButton(R.string.logout, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        SharedPreferences settings = getSharedPreferences(GALLERY_APP_PREFS, MODE_PRIVATE);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.remove(AUTH_TOKEN);
                        editor.commit();
                        CookieManager.getInstance().removeAllCookies(new ValueCallback<Boolean>() {
                            @Override
                            public void onReceiveValue(Boolean value) {

                            }
                        });
                        finish();
                    }
                })
                .setNegativeButton(R.string.cancel, null)
                .show();

    }

}
