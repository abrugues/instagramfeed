package ch.abrugues.instagramfeed.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ch.abrugues.instagramfeed.R;
import ch.abrugues.instagramfeed.dagger.InstagramFeedApplication;
import ch.abrugues.instagramfeed.model.DataUser;
import ch.abrugues.instagramfeed.model.User;
import ch.abrugues.instagramfeed.network.InstagramSvcApi;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileFragment extends Fragment {

    private final String TAG = getClass().getName();

    @BindView(R.id.profileProgressBar) ProgressBar mProgressBar;
    @BindView(R.id.avatarImgView) ImageView avatarImgView;
    @BindView(R.id.fullNameTxtView) TextView fullNameTxtView;
    @BindView(R.id.publicationsTxtView) TextView publicationsTxtView;
    @BindView(R.id.followersTxtView) TextView followersTxtView;
    @BindView(R.id.followingTxtView) TextView followingTxtView;

    private String authToken;

    @Inject
    InstagramSvcApi instagramSvcApi;

    public ProfileFragment() {

    }

    public static ProfileFragment newInstance(String authToken) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString(MainActivity.AUTH_TOKEN, authToken);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            authToken = getArguments().getString(MainActivity.AUTH_TOKEN);
        }

        ((InstagramFeedApplication) getActivity().getApplication()).getApplicationComponent().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (!TextUtils.isEmpty(authToken)) {
            fetchUserProfile();
        }
    }

    private void fetchUserProfile() {
        mProgressBar.setVisibility(ProgressBar.VISIBLE);
        Call<DataUser> dataUserCall = instagramSvcApi.getUserData(authToken);
        dataUserCall.enqueue(new Callback<DataUser>() {
            @Override
            public void onResponse(Call<DataUser> call, Response<DataUser> response) {
                mProgressBar.setVisibility(ProgressBar.GONE);
                if (response.isSuccessful()) {
                    User user = response.body().getUser();
                    // Fill the UI elements
                    Picasso.with(ProfileFragment.this.getContext())
                            .load(user.getProfilePicture())
                            .into(avatarImgView);

                    fullNameTxtView.setText(user.getFullName());

                    String publications = user.getCounts().getMediaCount() + " " + getString(R.string.publications);
                    publicationsTxtView.setText(publications);

                    String followers = user.getCounts().getFollowedBy() + " " + getString(R.string.followers);
                    followersTxtView.setText(followers);

                    String following = user.getCounts().getFollows() + " " + getString(R.string.following);
                    followingTxtView.setText(following);
                } else {
                    // Handle the case when the permission has been revoked
                    Intent i = new Intent(getActivity(), InstagramLoginActivity.class);
                    startActivityForResult(i, InstagramLoginActivity.AUTHENTICATE_USER);
                }

            }

            @Override
            public void onFailure(Call<DataUser> call, Throwable t) {

            }
        });
    }
}
