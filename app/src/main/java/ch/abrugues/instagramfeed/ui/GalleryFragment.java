package ch.abrugues.instagramfeed.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ch.abrugues.instagramfeed.R;
import ch.abrugues.instagramfeed.dagger.InstagramFeedApplication;
import ch.abrugues.instagramfeed.model.DataMedia;
import ch.abrugues.instagramfeed.model.Media;
import ch.abrugues.instagramfeed.network.InstagramSvcApi;
import ch.abrugues.instagramfeed.utils.RecyclerViewPhotoAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GalleryFragment extends Fragment {

    private final String TAG = getClass().getName();

    @BindView(R.id.progressBar) ProgressBar mProgressBar;
    @BindView(R.id.recycler_view) RecyclerView mRecyclerView;

    private GridLayoutManager mLayoutManager;
    private RecyclerViewPhotoAdapter mAdapter;
    private String authToken;

    @Inject
    InstagramSvcApi instagramSvcApi;


    public static GalleryFragment newInstance(String authToken) {
        GalleryFragment fragment = new GalleryFragment();
        Bundle args = new Bundle();
        args.putString(MainActivity.AUTH_TOKEN, authToken);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            authToken = getArguments().getString(MainActivity.AUTH_TOKEN);
        }

        ((InstagramFeedApplication) getActivity().getApplication()).getApplicationComponent().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gallery, container, false);
        ButterKnife.bind(this, view);
        mLayoutManager = new GridLayoutManager(getActivity(), 4);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new RecyclerViewPhotoAdapter(getActivity());
        mRecyclerView.setAdapter(mAdapter);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (!TextUtils.isEmpty(authToken)) {
            getUserMedia();
        }
    }

    private void getUserMedia() {
        mProgressBar.setVisibility(ProgressBar.VISIBLE);
        Call<DataMedia> mediaCall = instagramSvcApi.getUserSelfMedia(authToken);
        mediaCall.enqueue(new Callback<DataMedia>() {
            @Override
            public void onResponse(Call<DataMedia> call, Response<DataMedia> response) {
                mProgressBar.setVisibility(ProgressBar.GONE);
                if (response.isSuccessful()) {
                    List<Media> mediaList = response.body().getMedia();
                    mAdapter.addAll(mediaList);
                } else {
                    // Handle the case when the permission has been revoked
                    Intent i = new Intent(getActivity(), InstagramLoginActivity.class);
                    startActivityForResult(i, InstagramLoginActivity.AUTHENTICATE_USER);
                }
            }

            @Override
            public void onFailure(Call<DataMedia> call, Throwable t) {

            }
        });
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

}
