package ch.abrugues.instagramfeed.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import ch.abrugues.instagramfeed.R;


public class InstagramLoginActivity extends AppCompatActivity {

    private final String TAG = getClass().getName();

    static final int AUTHENTICATE_USER = 1;

    private String client_id;
    private String redirect_uri;
    private String auth_uri;
    private String authToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        client_id = getString(R.string.instagram_client_id);
        redirect_uri = getString(R.string.instagram_redirect_uri);
        auth_uri = "https://api.instagram.com/oauth/authorize/?client_id=" + client_id +
                "&redirect_uri=" + redirect_uri + "&response_type=token";

        WebView webView = new WebView(this);
        setContentView(webView);
        setClient(webView);

        webView.loadUrl(auth_uri);

    }

    private void setClient(WebView webView) {
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.startsWith(redirect_uri)) {
                    if (url.contains("access_token")) {
                        authToken = url.split("#access_token=")[1];
                        Intent i = new Intent();
                        i.putExtra(MainActivity.AUTH_TOKEN, authToken);
                        setResult(Activity.RESULT_OK, i);
                        finish();
                    }
                    return true;
                }
                return super.shouldOverrideUrlLoading(view, url);
            }
        });
    }
}

