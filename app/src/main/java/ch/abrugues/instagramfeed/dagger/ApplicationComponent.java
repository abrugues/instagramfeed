package ch.abrugues.instagramfeed.dagger;

import ch.abrugues.instagramfeed.ui.GalleryFragment;
import ch.abrugues.instagramfeed.ui.ProfileFragment;
import dagger.Component;

@Component(modules = NetworkModule.class)
public interface ApplicationComponent {

    void inject(GalleryFragment fragment);
    void inject(ProfileFragment fragment);
}
