package ch.abrugues.instagramfeed.utils;


import android.content.Context;
import android.content.Intent;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ch.abrugues.instagramfeed.R;
import ch.abrugues.instagramfeed.model.Images;
import ch.abrugues.instagramfeed.model.Media;
import ch.abrugues.instagramfeed.ui.GalleryFragment;
import ch.abrugues.instagramfeed.ui.InstagramImageActivity;
import ch.abrugues.instagramfeed.ui.MainActivity;

public class RecyclerViewPhotoAdapter extends RecyclerView.Adapter<RecyclerViewPhotoAdapter.ViewHolder>{

    private final String TAG = getClass().getName();

    private Context mContext;
    private List<Media> mediaList;

    public RecyclerViewPhotoAdapter(Context context) {
        mContext = context;
        mediaList = new ArrayList<>();
    }

    public void addAll(List<Media> mediaList) {
        this.mediaList.addAll(mediaList);
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_thumbnail, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Media media = mediaList.get(position);
        holder.setMedia(media);

        String imageUrl = media.getImages().getLowResolutionImage().getUrl();
        Picasso.with(mContext).load(imageUrl).into(holder.mThumbnailImageView);

    }

    @Override
    public int getItemCount() {
        return mediaList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.thumbnailImgView) ImageView mThumbnailImageView;

        private Media media;

        private ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            mThumbnailImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    MainActivity activity = (MainActivity) mContext;
                    Intent i = InstagramImageActivity.createIntent(mContext, media);
                    // Shared element Activity transition
                    Pair<View, String> pair = Pair.create((View) mThumbnailImageView, "image_shared");
                    ActivityOptionsCompat options = ActivityOptionsCompat
                            .makeSceneTransitionAnimation(activity, pair);

                    mContext.startActivity(i, options.toBundle());
                }
            });
        }

        private void setMedia(Media media) {
            this.media = media;
        }
    }
}
