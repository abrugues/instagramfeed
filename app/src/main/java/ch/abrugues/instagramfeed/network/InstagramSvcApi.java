package ch.abrugues.instagramfeed.network;


import ch.abrugues.instagramfeed.model.DataMedia;
import ch.abrugues.instagramfeed.model.DataUser;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface InstagramSvcApi {

    String INSTAGRAM_API_BASE_URL = "https://api.instagram.com/v1/";
    String USER_SVC = "users/self/";
    String USER_MEDIA_SVC = "users/self/media/recent/";

    String TOKEN = "access_token";

    @GET(USER_SVC)
    Call<DataUser> getUserData(@Query(TOKEN) String access_token);

    @GET(USER_MEDIA_SVC)
    Call<DataMedia> getUserSelfMedia(@Query(TOKEN) String access_token);

}
